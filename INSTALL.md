Building Raven
================

See doc/build-*.md for instructions on building the various
elements of the IDA Core reference implementation of Raven.
